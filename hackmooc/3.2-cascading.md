# Intro

* [ ] hierarquia
* [ ] cascateamento

## Hierarquia

Lembra-se do DOM, aquela árvore que o navegador gera ao interpretar o documento
HTML? Com essa estrutura fica fácil de ver que há uma hierarquia na página e
isso influencia a aplicação do CSS.

```html
<div class="root">
  algum texto
  <div class="grandma">
    algum texto
    <div class="mom">
      algum texto
      <div class="kid">
        algum texto
        <div class="leaf">
          algum texto
        </div>
      </div>
    </div>
  </div>
</div>
```

O DOM gerado por este HTML é

```plain
+-------------+ +- innerText = "algum texto"
|   div.root  |-+
+-------------+ |
       ||
+-------------+ +- innerText = "algum texto"
| div.grandma |-+
+-------------+ |
       ||
+-------------+ +- innerText = "algum texto"
|   div.mom   |-+
+-------------+ |
       ||
+-------------+ +- innerText = "algum texto"
|   div.kid   |-+
+-------------+ |
     ||
+-------------+ +- innerText = "algum texto"
|   div.leaf  |-+
+-------------+ |
```

## Cascateamento

```css
.root {
  font-size: 18px;
}

.grandma {
  color: red;
}

.mom {
  font-size: 24px;
}

.kid {
  color: green;
}

.leaf {
  font-size: 30px;
}
```

Após a aplicação do CSS, teeremos:

```plain
+-------------+ +- innerText = "algum texto"
|   div.root  |-+
+-------------+ +- font-size = 18px (cascateia para baixo)
       ||
+-------------+ +- innerText = "algum texto"
| div.grandma |-+- color = red (cascateia para baixo)
+-------------+ +- font-size = 18px
       ||
+-------------+ +- innerText = "algum texto"
|   div.mom   |-+- color = red
+-------------+ +- font-size = 24px (novo cascateia para baixo)
       ||
+-------------+ +- innerText = "algum texto"
|   div.kid   |-+- color = green (novo cascateia para baixo)
+-------------+ +- font-size = 24px
     ||
+-------------+ +- innerText = "algum texto"
|   div.leaf  |-+- color = green
+-------------+ +- font-size = 30px (novo cascateia para baixo)
```

Não são todas as propriedades que têm esse comportamento, mas é possível intuir
quais terão esse efeito cascata. Mais para frente falaremos um pouco mais sobre
Box Model, mas eu trago uma de suas propriedades: o `padding` não tem esse
comportamento de cascata para os filhos (por curiosidade, o `padding` determina
um espaçamento entre a borda do objeto e o conteúdo interno).

